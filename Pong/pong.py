"""
    Author: Kamil Szpilak
    Title: Pong
    Script description:
        Pong is a script which allows to scan local network in search of active hosts.
        Script is based on usage of 'ping' program. Pong uses threading, so speed of scanning is increased.
        Pong takes as input arguments (all arguments are optional):
            -f <number>     - sets given number as a value of last octet -> scan should start from 192.168.1.<number>
            -t <number>     - sets given number as a value of last octet -> scan should end at 192.168.1.<number>
            -c <number>     - sets number of pings per address
            -p <number>     - sets size of sent packet
            -l <number>     - sets TTL of packet
            -j              - changes the output style.
            -h              - displays help.
        If there are no arguments given then Pong will scan whole local network (192.168.1.1 - 192.168.1.255).
        Note that, some of devices (especially with Windows OS) may not be detected which is caused by firewall.
"""

import subprocess
import sys
import getopt
import time
import platform
import concurrent.futures

# Scan stats
total = 0
up = 0
up_addresses = list()
exe_time = 0


def main(argv):
    """
    Core of the script. Arguments are resolved here. Then the function validates input and sets correct prefixes of
    ping program. The next step is start scanning.
    :param argv: Arguments given by user
    """
    global up_addresses, exe_time

    from_val = 1
    to_val = 255
    just_result = False
    show_cmd = False
    count = 2
    packetsize = 32
    ttl = 50

    try:
        opts, args = getopt.getopt(argv, "hjsc:p:l:f:t:", ["", "", "count=", "packetsize=", "ttl=", "from=", "to="])
    except getopt.GetoptError:
        print('Argument ERROR : Invalid syntax.')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(
                'Usage: python pong.py -f <first_host> -t <last_host> -c <count> -p <packet_size> -l <ttl> -js')
            print('Note: All of arguments are optional.')
            print('Arguments description: ')
            print(
                '-f <number>     - sets given number as a value of last octet -> scan should start from 192.168.1.<number>')
            print(
                '-t <number>     - sets given number as a value of last octet -> scan should end at 192.168.1.<number>')
            print('-c <number>     - sets number of pings per address')
            print('-p <number>     - sets size of sent packet')
            print('-l <number>     - sets TTL of packet')
            print('-j              - changes the output style')
            print('-s              - shows executed command')
            print('-h              - displays help.')
            sys.exit(0)
        elif opt == '-j':
            just_result = True
        elif opt == '-s':
            show_cmd = True
        elif opt == '-c':
            count = arg
        elif opt == '-p':
            packetsize = arg
        elif opt == '-l':
            ttl = arg
        elif opt == '-f':
            from_val = arg
        elif opt == '-t':
            to_val = arg

    from_val, to_val = validate_input(from_val, to_val, count, packetsize, ttl)
    ping_opts = set_correct_pfx()

    ip_gen = lambda x: '192.168.1.{}'.format(x)

    print('Scan started. Range from {} to {}. Pings/host: {}'.format(ip_gen(from_val), ip_gen(to_val), count))
    start = time.time()

    # Prepare list of args
    args = []
    for host in range(from_val, to_val):
        current_ip = ip_gen(host)
        args.append([current_ip, count, packetsize, ttl, just_result, show_cmd, ping_opts])

    try:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(ping, args)
    except KeyboardInterrupt:
        pass
    finally:
        exe_time = time.time() - start


def validate_input(from_val, to_val, count, packetsize, ttl):
    """
    Function is used to validate user's input.
    :param from_val: -f value passed by user
    :param to_val: -t value passed by user
    :param count: -c value passed by user
    :param packetsize: -p value passed by user
    :param ttl: -l value passed by user
    :return: Corrected from_val and to_val
    """

    if not str(from_val).isdigit():
        print('Argument ERROR: Option -f takes digit as input.')
        sys.exit(2)

    if not str(to_val).isdigit():
        print('Argument ERROR: Option -t takes digit as input.')
        sys.exit(2)

    if int(from_val) > int(to_val):
        print('Warning: Input of -f is greater than input of -t. Swapping...')
        from_val, to_val = to_val, from_val

    if not str(count).isdigit() or not str(packetsize).isdigit() or not str(ttl).isdigit():
        print('Argument ERROR : Given value is not a digit.')
        sys.exit(2)

    return int(from_val), int(to_val)


def set_correct_pfx():
    """
    Function sets correct prefixes. It is based on user's OS.
    :return: Dictionary of correct prefixes
    """
    ping_opts = {'count': '', 'packetsize': '', 'ttl': ''}
    system = platform.system()

    if system == 'Windows':
        ping_opts['count'] = '-n'
        ping_opts['packetsize'] = '-l'
        ping_opts['ttl'] = '-i'
        ping_opts['finder'] = 'findstr /I'
        ping_opts['finder_arg'] = '/C:'
    elif system == 'Linux' or system == 'Darwin':
        ping_opts['count'] = '-c'
        ping_opts['packetsize'] = '-s'
        ping_opts['ttl'] = '-t'
        ping_opts['finder'] = 'grep -i'
        ping_opts['finder_arg'] = '-e '
    else:
        print('Internal ERROR : Unable to resolve OS name.')
        sys.exit(2)

    return ping_opts


def ping(vals):
    """
    Function uses ping program and gets the result.
    :param vals: list of arguments
    """
    global total, up, up_addresses

    ip, count, packetsize, ttl, just_result, show_cmd, ping_opt = vals
    cmd_line = f'ping {ip} {ping_opt.get("count")} {count} {ping_opt.get("packetsize")} {packetsize} {ping_opt.get("ttl")} {ttl} | {ping_opt.get("finder")} {ping_opt.get("finder_arg")}"100% packet loss" {ping_opt.get("finder_arg")}"timed out" {ping_opt.get("finder_arg")}"host unreachable" {ping_opt.get("finder_arg")}"could not find host"'

    if show_cmd:
        print(cmd_line.split('|')[0])
    command = subprocess.Popen(cmd_line, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
    out, err = command.communicate()

    total += 1
    if out == b'':
        if not just_result:
            print('{} is up.'.format(ip))
        else:
            up_addresses.append(ip)
        up += 1
    if err is not None:
        print(err)


def print_result():
    """
    Function prints out the results of script.
    """
    print('Total addresses scanned: {}.'.format(total))
    print('Active hosts: {}.'.format(up))
    if len(up_addresses) != 0:
        print(up_addresses)
    print('Done in ' + str(round(exe_time, 2)) + 's.')


if __name__ == '__main__':
    main(sys.argv[1:])
    print_result()
